#summary All the problems we encountered and how we solved them
#labels problem problems solution solutions

= Data not respecting SGML principles =

The first problem encountered was the data itself; even though it's described as "standard SGML format", it lacks a DTD description and, more importantly, a declaration at the beginning of the data files.
SOLVING...