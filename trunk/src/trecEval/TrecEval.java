package trecEval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.benchmark.quality.trec.TrecJudge;
import org.apache.lucene.benchmark.quality.trec.TrecTopicsReader;

public class TrecEval {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		TrecTopicsReader topicsReader = new TrecTopicsReader();
		
		QualityQuery[] qualityQueries = topicsReader.readQueries(new BufferedReader(new FileReader(new File("/Users/Fredd/Documents/workspace/ziff/trecEval/ZIFF-queries"))));;
		
		PrintWriter printWriter = new PrintWriter(new File("/Users/Fredd/Documents/workspace/ziff/trecEval/ZIFF-eval"));
			
		TrecJudge judger = new TrecJudge(new BufferedReader(new FileReader(new File("/Users/Fredd/Documents/workspace/ziff/trecEval/ZIFF-qrels"))));
		
		for (QualityQuery qualityQuery : qualityQueries) {
			System.out.println(qualityQuery.getQueryID());
			for (String queryName : qualityQuery.getNames()) {
				System.out.println(queryName.trim());
				System.out.println(qualityQuery.getValue(queryName).trim());
			}
			System.out.println("");
		}
		
		if(judger.validateData(qualityQueries, printWriter)) {
			
			System.out.println("\nvalid data");
			
		} else {
			
			System.out.println("\nnot valid data");
			
		}
		
	}

}
