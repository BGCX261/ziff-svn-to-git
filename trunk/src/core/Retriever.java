/**
 * 
 */
package core;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;

/**
 * @author adnan
 *
 */
public class Retriever {
	private Analyzer analyzer;
	public IndexSearcher is;
	private String indexPath;
	
	public Retriever(Analyzer analyzer,String indexPath){
		this.analyzer = analyzer;
		this.indexPath = indexPath;
	}
	
	public Hits retrieve(String myQueryString,String[] queryFields) {
		 try {
			is = new IndexSearcher(indexPath);			
			QueryParser	parser = new MultiFieldQueryParser(queryFields, analyzer);
			Query query = parser.parse(myQueryString);
			Hits hits = is.search(query);
			System.out.println("found "+hits.length()+ " matches");
//			is.close();
			return hits;
		} catch (CorruptIndexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
}
