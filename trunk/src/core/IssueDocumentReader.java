package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class IssueDocumentReader {
	
	private FileReader fileReader;
	private BufferedReader bufferedReader;
	private String fileName;
	private String docNumber;
//	private String docID;
	private String documentText;
	
	public IssueDocumentReader(String filePath, String docNumber) {
		this.docNumber = docNumber;
		this.fileName = filePath;
//		this.docID = id;
		this.documentText = "";
	}
	
	public String read() {
		try {
			fileReader = new FileReader(fileName);
			bufferedReader = new BufferedReader(fileReader);
			skipToWantedDoc();
			appendText();
			bufferedReader.close();
			fileReader.close();
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return documentText;
	}
	
	private void skipToWantedDoc() throws IOException {
		String s = "";
		while(!((s = bufferedReader.readLine()).startsWith("<" + Constants.DOCUMENTNUMBER.toUpperCase() + ">"))) {
//			do nothing
		}
		if(s.contains(docNumber)) {
			documentText += s + "\n";
		} else skipToWantedDoc();
	}
	
	private void appendText() throws IOException {
		String s = "";
		while(!(s = bufferedReader.readLine()).endsWith("</" + Constants.DOCUMENT + ">")) {
			documentText += s.replaceAll("&O", "").replaceAll("&M", "").replaceAll("&P", "") + "\n";
//			System.out.println(documentText);
		}
		documentText += s + "\n";
	}
}
