package core;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.PorterStemFilter;
import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

public class IssueAnalyzer extends Analyzer {
	
	private String[] stopWords;
	
	public IssueAnalyzer() {
		super();
		stopWords = StopAnalyzer.ENGLISH_STOP_WORDS;
	}
	
	@Override
	public TokenStream tokenStream(String field, Reader reader) {
		TokenStream result = new StandardTokenizer(reader);
		result = new StandardFilter(result);
		result = new LowerCaseFilter(result);
		result = new StopFilter(result, stopWords);
		result = new PorterStemFilter(result);
		return result;
	}
	
}
