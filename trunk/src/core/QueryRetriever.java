/**
 * 
 */
package core;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;

/**
 * @author adnan
 *
 */
public class QueryRetriever {
	private Analyzer analyzer;
	public IndexSearcher searcher;
	private String indexPath;
	private static int number_of_hits_per_page;
	
	public QueryRetriever(Analyzer analyzer, String indexPath,int num_of_link_per_page){
		this.analyzer = analyzer;
		this.indexPath = indexPath;
		this.number_of_hits_per_page = num_of_link_per_page;
		
	}
	
	public Hits retrieve(String myQueryString,String[] queryFields) {
		 try {
			searcher = new IndexSearcher(indexPath);			
			QueryParser	parser = new MultiFieldQueryParser(queryFields, analyzer);
			Query query = parser.parse(myQueryString);
			Hits hits = searcher.search(query);
			
			System.out.println("found "+ hits.length() + " matches");
//			is.close();
			return hits;
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;

	}
}
