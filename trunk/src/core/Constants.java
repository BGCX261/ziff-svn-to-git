package core;

public class Constants {

	public static final String DATAPATH = "/Users/Fredd/Documents/workspace/ziff/docs/";
	public static final String HTML_DATAPATH = "/Users/Fredd/Documents/workspace/ziff/html_docs/";
	
	public static final String DOCUMENT = "doc";
	public static final String DOCUMENTNUMBER = "docno";
	public static final String DOCUMENTID = "docid";
	public static final String TEXTCONTENT = "text";
	public static final String JOURNAL = "journal";
	public static final String AUTHOR = "author";
	public static final String TITLE = "title";
	public static final String DESCRIPT = "descript";
	public static final String ABSTRACT = "abstract";
	
	public static final String FILEPATH = "filePath";
	
	public static final String[] allowedTags = {DOCUMENT, DOCUMENTNUMBER, DOCUMENTID, TEXTCONTENT, JOURNAL, AUTHOR, TITLE, DESCRIPT, ABSTRACT};
	public static final String[] queryTags = allowedTags;
	public static final Integer PREVIEWTEXTSIZE = 180;


}
