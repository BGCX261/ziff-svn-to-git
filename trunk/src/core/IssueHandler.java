package core;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.io.*;

import org.xml.sax.*;

public class IssueHandler extends DefaultHandler {
	private static String[] allowedTags = Constants.allowedTags;
	static public Writer  out;
	private Document currentDoc = new Document();
	private IndexWriter indexWriter;
	private boolean canRead = false;
	private String filePath;
	private String text = "";
	
	public IssueHandler(String fileName, IndexWriter index){
		this.indexWriter = index;
		this.filePath = fileName;
	}
//	*
//	* Called when the Parser starts parsing the Current XML File.
//	*/
	public void startDocument () throws SAXException
	{
//		System.out.println("begin of document");
	}

	/*
	 *Called when the Parser Completes parsing the Current XML File.
	*/
	public void endDocument () throws SAXException
	{
//		System.out.println("end of document");
	}

	/*
	 * Called when the starting of the Element is reached. For Example if we have Tag
	 * called <Title> ... </Title>, then this method is called when <Title> tag is
	 * Encountered while parsing the Current XML File. The AttributeList Parameter has
	 * the list of all Attributes declared for the Current Element in the XML File.
	*/
	public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException
	{

		elementCase(qName, false);
	}

	/*
	 * Called when the Ending of the current Element is reached. For example in the
	 * above explanation, this method is called when </Title> tag is reached
	*/
	public void endElement (String uri, String localName, String qName) throws SAXException
	{

		elementCase(qName, true);
	}

	/*
	 * While Parsing the XML file, if extra characters like space or enter Character
	 * are encountered then this method is called. If you don't want to do anything
	 * special with these characters, then you can normally leave this method blank.
	*/
	public void characters (char buf [], int offset, int len) throws SAXException
	{
		if(canRead){
			String data = new String(buf);
			text += (data.substring(offset, offset + len));
		}
	}

	/*
	 * In the XML File if the parser encounters a Processing Instruction which is
	 * declared like this  <?ProgramName:BooksLib QUERY="author, isbn, price"?> 
	 * Then this method is called where Target parameter will have
	 * "ProgramName:BooksLib" and data parameter will have  QUERY="author, isbn,
	 *  price". You can invoke a External Program from this Method if required. 
	*/
	public void processingInstruction (String target, String data) throws SAXException
	{
	
	} 
	
    
    private void elementCase(String tag, boolean isEndTag) {
    	if (allowedToRead(tag)) {
			if(isEndTag) {
				canRead = false;
				try {
					writeInDocument(tag, false);
				} catch (CorruptIndexException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				canRead = true;
				text = "";
				try {
					writeInDocument(tag, true);
				} catch (CorruptIndexException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		} else {

		}
    }
    
    private void writeInDocument(String tag, boolean isDoc) throws CorruptIndexException, IOException {
    	if(tag.toLowerCase().equals(Constants.DOCUMENT) && isDoc) {
    		currentDoc = new Document();
    		currentDoc.add(new Field(Constants.FILEPATH, this.filePath, Field.Store.YES,Field.Index.UN_TOKENIZED));
    	} else if(tag.toLowerCase().equals(Constants.DOCUMENT) && !isDoc) {
    		indexWriter.addDocument(currentDoc);
    	} else if(tag.toLowerCase().equals(Constants.DOCUMENTNUMBER) && !isDoc) {
    		currentDoc.add(new Field(tag.toLowerCase(), this.text.trim(), Field.Store.YES, Field.Index.UN_TOKENIZED));
    	} else if(tag.toLowerCase().equals(Constants.DOCUMENTID) && !isDoc) {
    		currentDoc.add(new Field(tag.toLowerCase(), this.text.trim(), Field.Store.YES, Field.Index.UN_TOKENIZED));
    	} else if(tag.toLowerCase().equals(Constants.TITLE) && !isDoc) {
    		currentDoc.add(new Field(tag.toLowerCase(), this.text.trim(), Field.Store.YES, Field.Index.TOKENIZED));
    	} else if(tag.toLowerCase().equals(Constants.AUTHOR) && !isDoc) {
    		currentDoc.add(new Field(tag.toLowerCase(), this.text.trim(), Field.Store.YES, Field.Index.TOKENIZED));
    	} else if(tag.toLowerCase().equals(Constants.TEXTCONTENT) && !isDoc) {
    		currentDoc.add(new Field(tag.toLowerCase(), this.text.trim(), Field.Store.NO, Field.Index.TOKENIZED));
    	} else if(tag.toLowerCase().equals(Constants.ABSTRACT) && !isDoc) {
    		currentDoc.add(new Field(tag.toLowerCase(), this.text.trim(), Field.Store.NO, Field.Index.TOKENIZED));
    	} else if(tag.toLowerCase().equals(Constants.JOURNAL) && !isDoc) {
    		currentDoc.add(new Field(tag.toLowerCase(), this.text.trim(), Field.Store.YES, Field.Index.TOKENIZED));
    	} else if(tag.toLowerCase().equals(Constants.DESCRIPT) && !isDoc) {
    		currentDoc.add(new Field(tag.toLowerCase(), this.text.trim(), Field.Store.YES, Field.Index.TOKENIZED));
    	}
    }
    
    private boolean allowedToRead(String tag) {
    	for(int i = 0; i < allowedTags.length; i++) {
    		if(tag.toLowerCase().startsWith(allowedTags[i])) {
    			return true;
    		}
    	}
    	return false;
    }
	public IndexWriter getIndexWriter() {
		return indexWriter;
	}
}
