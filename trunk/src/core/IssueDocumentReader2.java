/**
 * 
 */
package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author adnan
 *
 */
public class IssueDocumentReader2 {
	private FileReader fr;
	private BufferedReader br;
	private String fileName;
	private String docNumber;
	private String documentText;
	
	public IssueDocumentReader2(String filePath,String docNumber) {
		this.docNumber = docNumber;
		this.fileName = filePath;
		this.documentText = "<DOC>\n";
		
	}
	
	public String read(){
		try {
			fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			skipToWantedDoc();
			appendText();
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return documentText;
	}
	
	private void skipToWantedDoc() throws IOException{
		String s = "";
		while(!((s = br.readLine()).startsWith("<DOCNO>"))){
			
		}
		if(s.contains(docNumber)){
			documentText += s + "\n";
		} else skipToWantedDoc();
	}
	private void appendText() throws IOException{
		String s ="";
		while(!(s=br.readLine()).endsWith("</DOC>")){
			documentText += s + "\n";
		}
		documentText += s + "\n";
	}
}
