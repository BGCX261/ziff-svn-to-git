package core;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.LockObtainFailedException;

public class IssueIndexer {
	
	private Analyzer analyzer;
	private IndexWriter writer;
	private final File INDEX_DIR = new File("IssueIndex");
	private File docDir;
	private int filesRead = 0;
	private int filesToRead = 250;
	private String currentFile;
	
	public IssueIndexer(String dir) {
		docDir = new File(dir);
		analyzer = new IssueAnalyzer();
		try {
			writer = new IndexWriter(INDEX_DIR, analyzer, true);
		} catch(CorruptIndexException e) {
			e.printStackTrace();
		} catch(LockObtainFailedException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean startIndexing() {
		try {
			this.indexDocs(writer, docDir);
			writer.optimize();
			writer.close();
			return true;
		} catch(IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private void indexDocs(IndexWriter writer, File file) throws IOException {
		if(file.canRead() && !file.getName().contains(".svn")) {
			if(file.isDirectory()) {
				String[] files = file.list();
				filesToRead = file.listFiles().length;
				if(files != null) {
					for(int i=0; i<files.length; i++) {
						filesToRead--;
						if(files[i].toLowerCase().contains("zf")) {
							indexDocs(writer, new File(file, files[i]));
						}
					}
				}
			} else {
				if(file.getName().toLowerCase().contains("zf")) {
					currentFile = file.getName();
					filesRead++;
//					System.out.println("indexing " + file);
//					System.out.println("absolute path " + file);
					IssueParser parser = new IssueParser(file.getAbsolutePath(), writer);
					parser.startParse();
				}
			}
		}
	}
	
	public File getDocDir() {
		return docDir;
	}
	
	public int getFileRead() {
//		System.out.println("Files to read:\t" + filesToRead);
//		System.out.println("Files read:\t" + filesRead);
		return (int) (((float)filesRead/((float)filesToRead+(float)filesRead))*100);
	}
	
	public int getFilesToRead() {
		return filesToRead;
	}
	
	public String getCurrentFile() {
		return currentFile;
	}
	
}
