package core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import ui.retriever.HitComponent;

public class XML_HTML_Converter {
	public static String parse(String s){
		StringReader  reader = new StringReader(s);
		StringWriter writer = new StringWriter();
		TransformerFactory tFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer =
			      tFactory.newTransformer
			         (new javax.xml.transform.stream.StreamSource
			            ("style/ZIFF.xsl"));
			 transformer.transform(new javax.xml.transform.stream.StreamSource(reader),new javax.xml.transform.stream.StreamResult
//		            ( new FileOutputStream("docs/test.html")));
			(writer));
//			 writer.flush();
//			 writer.close();

		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	String convSHTML = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"+ writer.toString();
		return convSHTML;
	}
	
	public static URL saveHTMLandReturnURL(HitComponent component){
		IssueDocumentReader2 reader = new IssueDocumentReader2(component.getDoc().getField(Constants.FILEPATH).stringValue(),component.getDoc().getField(Constants.DOCUMENTNUMBER).stringValue());
		String s = reader.read();
//		XML_HTML_Converter conv = new XML_HTML_Converter();		
		URL url = null;
//		System.out.println("##################");
		try {
			File dir = new File(Constants.HTML_DATAPATH);
			if(!dir.isDirectory()){
				dir.mkdir();
			}
			File f = new File(Constants.HTML_DATAPATH+component.getDoc().getField(Constants.DOCUMENTNUMBER).stringValue()+".html");
			if(!f.exists()) {
				f.createNewFile();
				FileWriter fw = new FileWriter(f);

				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(XML_HTML_Converter.parse(s));
				bw.close();
				fw.close();

			}
			 url = new URL("file://"+Constants.HTML_DATAPATH+component.getDoc().getField(Constants.DOCUMENTNUMBER).stringValue()+".html");
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		return url;
	} 

}
