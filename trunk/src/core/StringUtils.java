package core;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class StringUtils {
	
	public static String[] multipleWordsSurroundings(String text, String query, int amount) {
				
		String queries[] = query.split(" ");
		
		String surroundings[] = new String[queries.length];
		
		for(int i=0; i<queries.length; i++) {
			surroundings[i] = wrapQuery(surroundings(text, queries[i], amount), queries[i], "b");
		}
		
		return surroundings;
		
	}
	
	public static String surroundings(String text, String query, int amount) {
		
		int preText = 0;
		int postText = 0;
		final int TEXTLENGHT = amount;
		
		
		String fileContent = text.toLowerCase();
		query = query.toLowerCase();
		
		if(fileContent.indexOf(query) == -1) {
			return "";
		}
		
		int outputStart = fileContent.substring(0, fileContent.indexOf(query)).lastIndexOf(">") + 1;
		int outputEnd = fileContent.substring(fileContent.indexOf(query)).indexOf("<") + fileContent.indexOf(query);
		
		String finalString = "";
		
		if(fileContent.indexOf(query) - outputStart > TEXTLENGHT) {
			outputStart = fileContent.indexOf(query) - TEXTLENGHT;
		} else {
			postText = TEXTLENGHT - (fileContent.indexOf(query) - outputStart);
		}
		
		if(outputEnd - (fileContent.indexOf(query) + query.length()) > TEXTLENGHT) {
			outputEnd = fileContent.indexOf(query) + query.length() + TEXTLENGHT;
		} else {
			preText = TEXTLENGHT - (outputEnd - fileContent.indexOf(query) + query.length());
		}
		
		outputEnd = outputEnd + postText;
		outputStart = outputStart - preText;
		
		if(outputStart < fileContent.substring(0, fileContent.indexOf(query)).lastIndexOf(">") + 1) {
			outputStart = fileContent.substring(0, fileContent.indexOf(query)).lastIndexOf(">") + 1;
		}
		
		if(outputEnd > fileContent.substring(fileContent.indexOf(query)).indexOf("<") + fileContent.indexOf(query)) {
			outputEnd = fileContent.substring(fileContent.indexOf(query)).indexOf("<") + fileContent.indexOf(query);
		}
		
		if(preText > 0) {
			outputStart = outputStart + fileContent.subSequence(outputStart, outputEnd).toString().indexOf(" ") + 1;
			finalString += "...";
		} else {
			outputStart = outputStart + fileContent.subSequence(outputStart, outputEnd).toString().indexOf(">") + 1;
		}
		if(postText > 0) {
			outputEnd = fileContent.subSequence(0, outputEnd).toString().lastIndexOf(" ");
			finalString += text.substring(outputStart, outputEnd) + "...";
		} else {
			finalString += text.substring(outputStart, outputEnd);
		}
		
		return finalString;
	}
	
	public static String wrapQuery(String text, String query, String wrapper) {
		
		String finalString = "";
		
		String fileContent = text.toLowerCase();
		query = query.toLowerCase();
		
		if(fileContent.indexOf(query) == -1) {
			return text;
		}
		
		finalString = text.substring(0, fileContent.indexOf(query)) + "<" + wrapper + ">" +
		              text.substring(fileContent.indexOf(query), fileContent.indexOf(query) + query.length()) + "</" + wrapper + ">" +
		              wrapQuery(text.substring(fileContent.indexOf(query) + query.length()), query, wrapper);
//		              text.substring(fileContent.indexOf(query) + query.length());
		
		return finalString;
		
	}
	
	public static String insertBreakLines(String text, int charNumber){
//		String title = hits.doc(i).getField(Constants.TITLE).stringValue().trim();;
		int carry_down_times = text.length()/charNumber;
		System.out.println("times: "+carry_down_times);
		String m1 = text;
		for(int j = 1; j <= carry_down_times;j++){
			System.out.println(j);
			m1 = text.substring((j-1)*charNumber , j*charNumber);
			m1 += "<br>" + text.substring(j*charNumber);
		}
		text = m1;
		return text;
		
	}
	
	public static ArrayList<Integer> findQuerys(String text,String query){
		StringTokenizer querys = new StringTokenizer(query);
		String[] querysArray = query.split("\\s");
		StringTokenizer strings = new StringTokenizer(text);
		ArrayList<Integer> distances = new ArrayList<Integer>();
		int pos = 0;
		while(strings.hasMoreElements()){
			String s = strings.nextToken();
//			while(querys.hasMoreElements()){
			for(int i = 0; i< querysArray.length; i++){
//				String sQuery = querys.nextToken();
				if(s.toLowerCase().contains(querysArray[i].toLowerCase())){
					System.out.println(s);
					distances.add(pos);
				}
//				System.out.println(querysArray[i]);
				
			}
//			System.out.println(s);
			pos++;
		}
		System.out.println("positions:"+distances);
		return distances;
	}
	
//	public static String decideForText(String text,String query,int maxWordDistance){
//		ArrayList<Integer> distances = findQuerys(text, query);
//		int first = 0;
//		for(int i = 0;i<distances.size();i++){
//			
//		}
//	}
	
	public static String generateHTML_link(String text,String open_tag,String close_tag,Integer number){
		String num ="";
		if(number != null) num += number +": "; 
		String h = "<html>"+open_tag+"<a href=\"\">";
		h +=  num + text;
		h += "</a>"+close_tag+"</html>";
		return h;
	}
	

	
}
