package core;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.LockObtainFailedException;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class IssueParser {
	
	private SAXParser parser;
	private SAXParserFactory factory = SAXParserFactory.newInstance();
	private IssueHandler handler;
	private String fileName;
	
	public IssueParser(String fileName, IndexWriter writer) throws CorruptIndexException, LockObtainFailedException, IOException {
		handler = new IssueHandler(fileName, writer);
		this.fileName = fileName;
//		startParse();
	}

	public void startParse() {
		try {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(new File(fileName), handler);
		} catch(ParserConfigurationException e) {
			e.printStackTrace();
		} catch(SAXParseException e) {
			System.out.println("in line:" + e.getLineNumber());
			e.printStackTrace();
		} catch(SAXException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public SAXParser getParser() {
		return parser;
	}
	
}
