package test;

import core.Constants;
import core.IssueDocumentReader2;

public class ResultStringTestCase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		IssueDocumentReader2 reader = new IssueDocumentReader2(Constants.DATAPATH + "ZF_001","ZF109-706-077");
		
		String fileContent = reader.read();
		
		String query = "Greenville";
		
		final int TEXTLENGHT = 100;
		
		int outputStart;
		int outputEnd;
		
		int preText = 0;
		int postText = 0;
		
		outputStart = fileContent.substring(0, fileContent.indexOf(query)).lastIndexOf(">") + 1;
		outputEnd = fileContent.substring(fileContent.indexOf(query)).indexOf("<") + fileContent.indexOf(query);
		
		if(fileContent.indexOf(query) - outputStart > TEXTLENGHT) {
			outputStart = fileContent.indexOf(query) - TEXTLENGHT;
		} else {
			postText = TEXTLENGHT - (fileContent.indexOf(query) - outputStart);
		}
		
		if(outputEnd - (fileContent.indexOf(query) + query.length()) > TEXTLENGHT) {
			outputEnd = fileContent.indexOf(query) + query.length() + TEXTLENGHT;
		} else {
			preText = TEXTLENGHT - (outputEnd - fileContent.indexOf(query) + query.length());
		}
		
		outputEnd = outputEnd + postText;
		outputStart = outputStart - preText;
		
		if(outputStart < fileContent.substring(0, fileContent.indexOf(query)).lastIndexOf(">") + 1) {
			outputStart = fileContent.substring(0, fileContent.indexOf(query)).lastIndexOf(">") + 1;
		}
		
		if(outputEnd > fileContent.substring(fileContent.indexOf(query)).indexOf("<") + fileContent.indexOf(query)) {
			outputEnd = fileContent.substring(fileContent.indexOf(query)).indexOf("<") + fileContent.indexOf(query);
		}
		
		if(preText > 0) {
			outputStart = outputStart + fileContent.subSequence(outputStart, outputEnd).toString().indexOf(" ") + 1;
			System.out.print("...");
		}
		if(postText > 0) {
			outputEnd = fileContent.subSequence(0, outputEnd).toString().lastIndexOf(" ");
			System.out.print(fileContent.substring(outputStart, outputEnd) + "...");
		} else {
			System.out.print(fileContent.substring(outputStart, outputEnd));
		}
		
	}

}