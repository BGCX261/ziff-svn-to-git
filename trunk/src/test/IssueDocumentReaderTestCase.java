/**
 * 
 */
package test;

import core.Constants;
import core.IssueDocumentReader;

public class IssueDocumentReaderTestCase {

	public static void main(String[] args) {
		IssueDocumentReader reader = new IssueDocumentReader(Constants.DATAPATH + "ZF_001","ZF109-706-077");
		reader.read();
	}

}
