package test;

import core.Constants;
import core.IssueDocumentReader2;
import core.StringUtils;

public class StringUtilsTextCase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		IssueDocumentReader2 reader = new IssueDocumentReader2(Constants.DATAPATH + "ZF_002","ZF109-732-833");

		String fileContent = reader.read();

		String query = "Microsoft";
		
//		String queries[] = query.split(" ");
//		
//		for (String string : queries) {
//			System.out.println(string);
//		}
		
//		String output = StringUtils.surroundings(fileContent, query, 200);
//		
//		System.out.println(output);
//		
//		output = StringUtils.wrapQuery(output, query, "b");
//		
//		System.out.println(output);
		
		String[] output = StringUtils.multipleWordsSurroundings(fileContent, query, 200);
		
		for (String element : output) {
			System.out.println(element);
			System.out.println("\n");
		}
		
		StringUtils.findQuerys(fileContent, "greenville Sequent");

	}

}
