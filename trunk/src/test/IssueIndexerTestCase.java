package test;

import java.io.File;

import core.Constants;
import core.IssueIndexer;

public class IssueIndexerTestCase {
	private static File path;
	private static IssueIndexer indexer;
	
	public static void main(String[] args) {
		
		path = new File(Constants.DATAPATH);
		
		indexer = new IssueIndexer(path.getPath());
		
		boolean success = indexer.startIndexing();
		
		if(success) {
			System.out.println(">>indexing complete");
		} else {
			System.out.println(">>indexing failed");
		}
		
	}
}
