package test;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.search.Hits;
import core.Constants;
import core.IssueAnalyzer;
import core.QueryRetriever;

public class IssueRetrieverTestCase {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 */
	public static void main(String[] args) throws CorruptIndexException, IOException {
		String query = "processor";
		
		String[] queryFields = Constants.queryTags;
		
		Hits hits;
		Analyzer analyzer = new IssueAnalyzer();				
		QueryRetriever queryRetriever = new QueryRetriever(analyzer, "./IssueIndex",20);

		hits = queryRetriever.retrieve(query, queryFields);
		
		for(int i=0; i<50 && i<hits.length(); i++) {
			System.out.println(hits.doc(i).getField(Constants.DOCUMENTNUMBER).stringValue());
			System.out.println(hits.doc(i).getField(Constants.TITLE).stringValue() + "\n");
		}
	}

}
