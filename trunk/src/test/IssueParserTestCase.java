/**
 * 
 */
package test;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.store.LockObtainFailedException;

import core.Constants;
import core.IssueAnalyzer;
import core.IssueParser;
import core.QueryRetriever;

public class IssueParserTestCase {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws ParseException {

		final File INDEX_DIR = new File("IssueIndex");
		try {
			Analyzer analyzer = new IssueAnalyzer();
			IndexWriter writer= new IndexWriter(INDEX_DIR, analyzer, true);

			IssueParser parser = new IssueParser(Constants.DATAPATH + "ZF_001", writer);
			//			IssueParser parser_2 = new IssueParser(Constants.DATAPATH + "ZF_002", writer);

			QueryRetriever retriever = new QueryRetriever(analyzer, "./IssueIndex",20);
			String[] queryFields = {Constants.JOURNAL};
			retriever.retrieve("Business", queryFields);
			parser.startParse();
			writer.optimize();
			writer.close();
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (LockObtainFailedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
