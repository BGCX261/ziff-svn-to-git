package ui.retriever;

import java.awt.Desktop;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.search.Hits;

import core.Constants;
import core.IssueAnalyzer;
import core.IssueDocumentReader2;
import core.QueryRetriever;
import core.StringUtils;
import core.XML_HTML_Converter;

public class RetrieverPanel extends JPanel{
	private String[] queryFields = Constants.queryTags;
	private static final long serialVersionUID = 1L;
	public String query;
	JTextField searchField;
	JTextField mustIncludeSearchField;
	JTextField shouldIncludeSearchField;
	JTextField mustNotIncludeSearchField;
	public  JScrollPane scrollPane;
	public JScrollPane scrollPaneDocument;
	Hits hits;
	QueryRetriever retriever;
	public  JPanel bottom;
	public  JEditorPane documentPane;
	public JPanel components;
	public String DOCNUMBER = "";

	public RetrieverPanel() {
		JPanel top = new JPanel();
		setLayout(new BorderLayout());
		searchField = new JTextField(45);
		top.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY),
				"Search for..." , TitledBorder.LEFT,
				TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 14),
				Color.BLACK));
		top.setLayout(new BorderLayout());
		
		searchField.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {

			}

			public void keyReleased(KeyEvent e) {

			}

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar() == KeyEvent.VK_ENTER){
					search(searchField.getText());
				}
			}

		});

		top.add(searchField, BorderLayout.CENTER);

		JButton browse = new JButton("Search");
		browse.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				search(searchField.getText());
			}


		});

		JButton advSearch = new JButton("Advanced Search");
		advSearch.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				search(searchField.getText());
			}


		});
		top.add(browse, BorderLayout.EAST);
		add(top, BorderLayout.NORTH);


		bottom = new JPanel();
		bottom.setPreferredSize(new  Dimension(650,2000));
		bottom.setMaximumSize(new  Dimension(2000,2000));
		
		components = new JPanel();
		components.setLayout(new BoxLayout(components,BoxLayout.Y_AXIS));

		scrollPaneDocument = new JScrollPane();
		scrollPaneDocument.setMinimumSize(new  Dimension(600,600));

		documentPane = new JEditorPane();
		documentPane.setMinimumSize(new  Dimension(600,600));
		documentPane.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY),
				"Preview Document: "+ DOCNUMBER, TitledBorder.LEFT,
				TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 14),
				Color.BLACK));
		
		bottom.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY),
				"Results for..", TitledBorder.LEFT,
				TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 14),
				Color.BLACK));

		bottom.add(components);
		scrollPane = new JScrollPane(components);
		scrollPane.setPreferredSize(new Dimension(650,650));
		scrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY),
				"Retrieved Documents", TitledBorder.LEFT,
				TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 14),
				Color.BLACK));
		
		add(scrollPane,BorderLayout.WEST);
		
		scrollPaneDocument.setVisible(true);
		scrollPaneDocument.getViewport().add(documentPane);
		add(scrollPaneDocument,BorderLayout.CENTER);

	}

	private void search(String query){
		this.query = query;
		Analyzer analyzer = new IssueAnalyzer();				
		retriever = new QueryRetriever(analyzer,"./IssueIndex",20);

		hits = retriever.retrieve(this.query, queryFields);
		try {
			updateResult();
		} catch (CorruptIndexException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		updateUI();
	}

	private void updateResult() throws CorruptIndexException, IOException{
		components.removeAll();
		components.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY),
				"Query: " + searchField.getText(), TitledBorder.LEFT,
				TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 14),
				Color.BLACK));
		int num = -1;
		if(hits != null){
			//displaying only the first 20 results at max
			for(int i = 0; i< hits.length() && i < 20;i++){
				
				final HitComponent component = new HitComponent(hits.doc(i));
				num++;
				component.setCursor(new Cursor(Cursor.HAND_CURSOR));
				final MyEditorPane descComponent = new MyEditorPane(num);
				Insets desc_insets = descComponent.getInsets();
				desc_insets.set(0, 5, 0, 0);
				descComponent.setMargin(desc_insets);
				descComponent.setContentType("text/html");
				descComponent.setText(StringUtils.generateHTML_link("see descrption", "<h4>", "</h4>", null));
				descComponent.setCursor(new Cursor(Cursor.HAND_CURSOR));


				final JPanel panel = new JPanel();
				panel.setLayout(new GridLayout(3, 1));

				IssueDocumentReader2 reader = new IssueDocumentReader2(component.getDoc().getField(Constants.FILEPATH).stringValue(),component.getDoc().getField(Constants.DOCUMENTNUMBER).stringValue());
				String xml = reader.read();
				String[] matchedArray = StringUtils.multipleWordsSurroundings(xml, this.query, Constants.PREVIEWTEXTSIZE);
				
				String matched = "";
				if(matchedArray.length>0) {
					matched = matchedArray[0];
					if(matchedArray.length>1){
						System.out.println("BIGGER");
						matched += "<separator>"+matchedArray[1];
					}
				}

				matched = StringUtils.insertBreakLines(matched, 90);
				final JEditorPane queryMatchedField = new JEditorPane();

				queryMatchedField.setMargin(desc_insets);
				queryMatchedField.setContentType("text/html");
				queryMatchedField.setText("<html><i>"+matched+"</i></html>");

				queryMatchedField.setEditable(false);
				descComponent.setEditable(false);
				
				if(i == 0){
					DOCNUMBER = component.getDoc().getField(Constants.DOCUMENTNUMBER).stringValue();
					URL url = XML_HTML_Converter.saveHTMLandReturnURL(component);
					try {
						documentPane.setPage(url);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				descComponent.addMouseListener(new MouseListener(){

					public void mouseClicked(MouseEvent e) {
						String desc = component.getDoc().getField(Constants.DESCRIPT).stringValue().trim();
						String desc2= desc.replaceAll("\\.\\s", ".<br>");
						
						descComponent.setText("<html><h4> Description: </h4><i>"+desc2+"</i></html>");
					}

					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub

					}

				});

				component.addMouseListener(new MouseListener() {

					public void mouseReleased(MouseEvent e) {}
					public void mousePressed(MouseEvent e) {}
					public void mouseExited(MouseEvent e) {}
					public void mouseEntered(MouseEvent e) {
						DOCNUMBER = component.getDoc().getField(Constants.DOCUMENTNUMBER).stringValue();
						URL url = XML_HTML_Converter.saveHTMLandReturnURL(component);
						try {
							documentPane.setPage(url);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						getRootPane().updateUI();
					}
					public void mouseClicked(MouseEvent e) {
						DOCNUMBER = component.getDoc().getField(Constants.DOCUMENTNUMBER).stringValue();
						try{
						URI uri = XML_HTML_Converter.saveHTMLandReturnURL(component).toURI();
						Desktop.getDesktop().browse(uri);
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						} 

						getRootPane().updateUI();
					}
				});


				String title = hits.doc(i).getField(Constants.TITLE).stringValue().trim();;
				title = StringUtils.insertBreakLines(title, 75);
				String html_title = StringUtils.generateHTML_link(title, "<h3>", "</h3>", (i+1));
				component.setText(html_title);
				panel.setLayout(new BorderLayout());
				panel.add(component,BorderLayout.NORTH);
				panel.add(queryMatchedField,BorderLayout.CENTER);
				panel.add(descComponent,BorderLayout.SOUTH);
				panel.setBorder(BorderFactory.createTitledBorder(BorderFactory
						.createLineBorder(Color.GRAY),
						"Document No: "+component.getDoc().getField(Constants.DOCUMENTNUMBER).stringValue().trim(), TitledBorder.LEFT,
						TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 10),
						Color.BLACK));
				
				components.add(panel);
			}
			retriever.searcher.close();
			num++;
		}

	}

	public void initDocumentFrame(String title,String docNumber) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame("" + title);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocation(250, 50);
		Dimension preferredSize = new Dimension(600, 800);
		frame.setPreferredSize(preferredSize);
		scrollPaneDocument = new JScrollPane();
		scrollPaneDocument.getViewport().add(documentPane);
		frame.getContentPane().add(scrollPaneDocument, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}
	
	public void changeDescComp(int num){
		JEditorPane descComponent = (JEditorPane) components.getComponent(num);
		String desc = ((HitComponent) ((JPanel)(components.getComponent(num - 1))).getComponent(0)).getDoc().getField(Constants.DESCRIPT).stringValue().trim();
		String desc2= desc.replaceAll("\\.\\s", ".<br>");
		
		descComponent.setText("<html><h4>&nbsp; Description: </h4><i> &nbsp;"+desc2+"</i></html>");
	}
}
