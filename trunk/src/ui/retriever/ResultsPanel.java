package ui.retriever;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.search.Hits;

import core.Constants;
import core.StringUtils;

public class ResultsPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public JScrollPane scrollPane;
	public JScrollPane scrollPaneDocument;
	Hits hits;
	String query;
	JPanel bottomLeft;
	int previewTextSize = Constants.PREVIEWTEXTSIZE;

	public ResultsPanel() {

		setLayout(new BorderLayout());

		bottomLeft = new JPanel();
		bottomLeft.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY),
				"Results for..", TitledBorder.LEFT,
				TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 14),
				Color.BLACK));
		scrollPane = new JScrollPane(bottomLeft);
		add(scrollPane, BorderLayout.CENTER);
	}

	public void fillPanel(Hits hits, String query) throws CorruptIndexException, IOException {

		this.hits = hits;
		this.query = query;

		updateResult();

	}

	private void updateResult() throws CorruptIndexException, IOException {
		bottomLeft.removeAll();
		JPanel components = new JPanel();
		components.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY),
				"Query: " + query, TitledBorder.LEFT,
				TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 14),
				Color.BLACK));
		components.setLayout(new GridLayout(0, 1));

		if(hits != null){
			for(int i = 0; i< hits.length();i++) {
				final HitComponent component = new HitComponent(hits.doc(i));
				component.setBackground(Color.YELLOW);
				component.setCursor(new Cursor(Cursor.HAND_CURSOR));
				component.addMouseListener(new MouseListener() {

					public void mouseReleased(MouseEvent e) {}
					public void mousePressed(MouseEvent e) {}
					public void mouseExited(MouseEvent e) {}
					public void mouseEntered(MouseEvent e) {}
					public void mouseClicked(MouseEvent e) {
//						do something
					}
				});
				String h = StringUtils.wrapQuery(StringUtils.surroundings(component.getDoc().toString(), this.query, this.previewTextSize), this.query, "b");
				component.setText(h);
				components.add(component, BorderLayout.WEST);
			}
		}

		bottomLeft.add(components);
		updateUI();
	}

}
