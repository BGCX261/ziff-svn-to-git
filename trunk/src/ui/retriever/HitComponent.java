/**
 * 
 */
package ui.retriever;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import org.apache.lucene.document.Document;

/**
 * @author adnan
 *
 */
public class HitComponent  extends JLabel {

//		private static final long serialVersionUID = 1L;
		private Document doc;
		
		public HitComponent(Document doc){
			super();
			this.doc = doc;
		}


		public Document getDoc() {
			return doc;
		}
}
