package ui.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;


public class ImagePanel extends JPanel {


	private static final long serialVersionUID = 1L;

	private Image background;
	private final ImageIcon icon;
	
	
	public ImagePanel(String image) {
		
		setBackground(Color.WHITE);
		icon = new ImageIcon(image);
		background = icon.getImage();
		setPreferredSize(new Dimension(background.getWidth(null),
				background.getHeight(null)));
		
	}
	
	
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		super.paintComponent(g2);
		g2.drawImage(background, 0, 0, null);
	}


	public void setBackground(String image) {
		ImageIcon icon = new ImageIcon(image);
		this.background = icon.getImage();
		updateUI();
	}
}
