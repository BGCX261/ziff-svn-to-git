package ui;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import ui.index.IndexerPanel;
import ui.retriever.RetrieverPanel;
import ui.util.IO;
import ui.util.ImagePanel;

public class MainPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public MainPanel() {
		setLayout(new BorderLayout());
		ImagePanel imagePanel = new ImagePanel(IO.IZIFF);
		add(imagePanel,BorderLayout.NORTH);
		
		JTabbedPane tabbedPane = new JTabbedPane();
		
		IndexerPanel indexerPanel = new IndexerPanel();		
		tabbedPane.addTab("Indexer", indexerPanel );
		
		
		RetrieverPanel retrieverPanel = new RetrieverPanel();
		tabbedPane.addTab("Retriever", retrieverPanel);
		
		
		
		add(tabbedPane, BorderLayout.CENTER);
		
		
		
	}
	
}
