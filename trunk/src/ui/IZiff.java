package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class IZiff implements WindowListener {

	public static final int WIDHT = 1200;

	public static final int HEIGHT = 800;

	public static JFrame frame;

	public void init() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		frame = new JFrame("IZiff");
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocation(50, 50);
		Dimension preferredSize = new Dimension(WIDHT, HEIGHT);
		frame.setPreferredSize(preferredSize);
		MainPanel mainPanel = new MainPanel();
		frame.getContentPane().add(mainPanel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
		frame.addWindowListener(this);
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				IZiff main = new IZiff();
				main.init();
			}
		});
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowClosing(WindowEvent e) {
		int n = JOptionPane.showConfirmDialog(frame,
				"Are you sure you want to quit IZiff", "IZiff Information.",
				JOptionPane.YES_NO_OPTION);
		if (JOptionPane.YES_OPTION == n)
			System.exit(0);

	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

}
