package ui.index;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import core.IssueIndexer;

import ui.IZiff;


public class IndexerPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextArea console;
	private File path;
	private JTextField pathField;
	private JProgressBar bar;
	private IssueIndexer indexer;
	private JPanel info1;
	private Integer numOfFiles = 250;
	boolean startIndexing;
	public IndexerPanel() {
		setLayout(new BorderLayout());
		JPanel top = new JPanel();
		top.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY),
				"Select File or Directory to Index", TitledBorder.LEFT,
				TitledBorder.CENTER, new Font("Helvetica", Font.BOLD, 14),
				Color.BLACK));
		top.setLayout(new BorderLayout());

		pathField = new JTextField(45);
		top.add(pathField, BorderLayout.CENTER);

		JButton browse = new JButton("browse");
		top.add(browse, BorderLayout.EAST);
		browse.addActionListener(new ActionListener() {



			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser(new File("~/"));
				fc.setForeground(Color.BLACK);
				fc.addChoosableFileFilter(new MyFilter());
				fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				fc.showOpenDialog(IZiff.frame);
				path = fc.getSelectedFile();
				if (path != null) {
					pathField.setText(path.getPath());
				}
			}

		});

		JPanel buttonRun = new JPanel();
		buttonRun.setLayout(new GridLayout(1, 3));
		JButton run = new JButton("Run");
		run.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				indexer = new IssueIndexer(path.getPath());
				new Thread(new Runnable() {

					public void run() {
						print(">> Start indexing "+ path.getPath());
						startIndexing = true;
						boolean success = indexer.startIndexing();
						numOfFiles = indexer.getFilesToRead();
						repaint();
						updateUI();
						if(success) {
							print(">> Indexing completed");
							startIndexing = false;
						}
						else print(">> Indexing failure");
					}

				}).start();




			}

		});
		new Thread(new Runnable() {

			public void run() {
				String oldPath = "";
				while(true){
					try {
						if(startIndexing){
							bar.setValue(indexer.getFileRead());
							if(oldPath != null && !oldPath.equals(indexer.getCurrentFile())){
								print(">> Indexing file "+ indexer.getCurrentFile());
								oldPath = indexer.getCurrentFile();
							}
							Thread.sleep(800);
						} else {
							Thread.sleep(800);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		}).start();
		buttonRun.add(new JLabel());
		buttonRun.add(run);
		buttonRun.add(new JLabel());
		top.add(buttonRun, BorderLayout.SOUTH);

		add(top, BorderLayout.NORTH);

		JScrollPane center = new JScrollPane();
		Color bg = new Color(232,232,232);
		center.setBackground(bg);
		center.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY), "Index Console",
				TitledBorder.LEFT, TitledBorder.CENTER, new Font("Helvetica",
						Font.BOLD, 14), Color.BLACK));

		console = new JTextArea(20, 20);
		console.setEditable(false);
		print(">> IZiff Index Writer is ready...");
		center.setViewportView(console);
		add(center, BorderLayout.CENTER);

		JPanel bottom = new JPanel();
		bottom.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createLineBorder(Color.GRAY), "Index Writer Information",
				TitledBorder.LEFT, TitledBorder.CENTER, new Font("Helvetica",
						Font.BOLD, 14), Color.BLACK));
		bottom.setLayout(new GridLayout(3, 1));

		info1 = new JPanel();
		info1.setLayout(new GridLayout(1, 2));
		//		int nrIndexedFiles = 0;

		info1.add(new JLabel("         Number of Files :"));
		info1.add(new JLabel("<html><strong>" + Integer.toString(numOfFiles)+"</strong></html>"));

		bottom.add(info1);

		JPanel info2 = new JPanel();
		info2.setLayout(new GridLayout(1, 2));
		int nrdocs = 75180;

		info2.add(new JLabel("         Number of Documents :"));
		info2.add(new JLabel("<html><strong>" + Integer.toString(nrdocs) + "</strong></html>"));

		bottom.add(info2);

		bar = new JProgressBar(0, 100);
		bar.setStringPainted(true);
		bar.setBorder(BorderFactory.createEtchedBorder());
		bottom.add(bar);

		add(bottom, BorderLayout.SOUTH);

	}


	public void print(String string) {
		if (!console.getText().trim().equals("")){
			console.setText(console.getText().concat("\n" + string));
			//			updateUI();
		} else {
			console.setText(string);
			//			updateUI();
		}
		updateUI();
	}

	class MyFilter extends javax.swing.filechooser.FileFilter {

		public boolean accept(File file) {
			return file.isDirectory() || file.getPath().contains("ZF_");
		}

		public String getDescription() {
			return "dir / ZIFF ";
		}
	}
}
