<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="/Users/Fredd/Documents/workspace/ziff/docs/ZIFF.css" type="text/css" />
                    
                <title><xsl:value-of select="DOC/TITLE"/></title>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="DOC/TITLE">
        <div class="doc" id="title"><br /><h3><xsl:apply-templates/></h3></div>
    </xsl:template>
    <xsl:template match="DOC/DOCNO">    
        <div class="doc" id="docno"><p>Doc Number:<i><xsl:apply-templates/></i></p></div>
    </xsl:template>
    <xsl:template match="DOC/DOCID">    
        <div class="doc" id="docid"><p>Doc Id:<i><xsl:apply-templates/></i></p></div>
    </xsl:template>
    <xsl:template match="DOC/JOURNAL">
        <div class="doc" id="journal"><p>Journal:<br /><i><xsl:apply-templates/></i></p></div>
    </xsl:template>
    <xsl:template match="DOC/TEXT/ABSTRACT | DOC/ABSTRACT">    
        <div class="doc" id="abstract"><p><i>Abstract</i><br /><i><xsl:apply-templates/></i></p></div>
    </xsl:template>
	<xsl:template match="DOC/TEXT/DESCRIPT | DOC/DESCRIPT">    
        <div class="doc" id="description"><p>Description<br /><i><xsl:apply-templates/></i></p></div>
    </xsl:template>
    <xsl:template match="DOC/AUTHOR">    
        <div class="doc" id="author"><p>Author:<i><xsl:apply-templates/></i></p></div>
    </xsl:template>
    <xsl:template match="DOC/TEXT">    
        <div class="doc" id="text"><p><h3>Content</h3><br /><i><xsl:apply-templates/></i></p></div>
    </xsl:template>
</xsl:stylesheet>